import {Component} from '@angular/core';
import {NotesService} from '../notes/notes.service';
import {ActivatedRoute, Router} from '@angular/router';
import {first} from 'rxjs/internal/operators';
import {INote} from '../model/notes';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
    selector: 'app-edit-note',
    templateUrl: './edit-note.component.html',
    styleUrls: ['./edit-note.component.scss']
})
export class EditNoteComponent {
    working = false;

    noteForm = this.createFormGroup({
       id: 0,
       title: ''
    });

    private editId:number;

    constructor(private notesService:NotesService, private route:ActivatedRoute, private router:Router) {
        const id = this.route.snapshot.paramMap.get('id');
        if (id !== null) {
            this.editId = parseInt(id);
            this.loadNote(this.editId);
        }
    }

    onSubmit() {
        this.working = true;

        if (this.editId === undefined) {
            this.addNote(this.noteForm.value);
        }
        else {
            this.editNote(this.editId, this.noteForm.value);
        }
    }

    onCancel() {
        this.backToHome();
    }

    private loadNote(id:number) {
        this.working = true;

        this.notesService.get(id)
            .pipe(first())
            .subscribe((data:INote) => {
                this.noteForm = this.createFormGroup(data);
                this.working = false;
            });
    }

    private addNote(note:INote) {
        this.notesService.add(note)
            .pipe(first())
            .subscribe(() => {
                this.backToHome();
                this.working = false;
            });
    }

    private editNote(id:number, note:INote) {
        this.notesService.edit(id, note)
            .pipe(first())
            .subscribe(() => {
                this.backToHome();
                this.working = false;
            });
    }

    private backToHome() {
        this.router.navigate(['']);
    }

    private createFormGroup(note:INote):FormGroup {
        return new FormGroup({
            title: new FormControl(note.title, Validators.required)
        });
    }
}
