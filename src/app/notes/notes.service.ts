import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/index';
import {INote} from '../model/notes';
import {HttpClient} from '@angular/common/http';

const HOST = 'http://private-9aad-note10.apiary-mock.com/';

@Injectable({
    providedIn: 'root'
})
export class NotesService {
    constructor(private http: HttpClient) {}

    list():Observable<INote[]> {
        return this.http.get(`${HOST}/notes`) as Observable<INote[]>;
    }

    get(id:number):Observable<INote> {
        return this.http.get(`${HOST}/notes/${id}`) as Observable<INote>;
    }

    add(note:INote):Observable<INote> {
        return this.http.post(`${HOST}/notes`, note) as Observable<INote>;
    }

    edit(id:number, note:INote):Observable<INote> {
        return this.http.put(`${HOST}/notes/${id}`, note) as Observable<INote>;
    }

    remove(id:number) {
        return this.http.delete(`${HOST}/notes/${id}`);
    }
}
