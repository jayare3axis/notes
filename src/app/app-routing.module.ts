import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {EditNoteComponent} from './edit-note/edit-note.component';

const routes:Routes = [{
    path: '',
    pathMatch: 'full',
    component: HomeComponent
}, {
    path: 'edit/:id',
    component: EditNoteComponent
}, {
    path: 'new',
    component: EditNoteComponent
}];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
