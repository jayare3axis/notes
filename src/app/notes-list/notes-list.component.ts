import {Component} from '@angular/core';
import {INote} from '../model/notes';
import {NotesService} from '../notes/notes.service';
import {first} from 'rxjs/internal/operators';
import {Router} from "@angular/router"

@Component({
    selector: 'app-notes-list',
    templateUrl: './notes-list.component.html',
    styleUrls: ['./notes-list.component.scss']
})
export class NotesListComponent {
    displayedColumns:string[] = ['id', 'title', 'edit'];
    dataSource:INote[] = [];

    constructor(private notesService:NotesService, private router:Router) {
        this.update();
    }

    onEdit(id:number) {
        this.router.navigate(['/edit', id]);
    }

    onRemove(id:number) {
        this.notesService.remove(id)
            .pipe(first())
            .subscribe(() => {
                // For simplicity we just reload the page
                window.location.reload(true);
            });
    }

    private update() {
        this.notesService.list()
            .pipe(first())
            .subscribe((data:INote[]) => {
                this.dataSource = data;
            });
    }
}
