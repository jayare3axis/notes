# Getting started
Install node.js, then clone this repository and navigate to project folder.
Run "npm install -g @angular/cli".
Run "npm install".

# Running app.

Navigate to project and run "ng serve". Go to your browser and navigate to `http://localhost:4200/`.

# Running unit tests

Navigate to project folder and run "ng test".
